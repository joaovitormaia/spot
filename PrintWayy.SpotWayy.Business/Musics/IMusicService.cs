﻿using PrintWayy.SpotWayy.DAO.Musics;
using PrintWayy.SpotWayy.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintWayy.SpotWayy.Business.Musics
{
    public interface IMusicService
    {
        IMusicsDAO MusicDao { get; }
        List<Music> Select(Albums album);
        bool Add(List<Music> musicList);
        bool Remove(Music musics);
        bool Edit(Music music);
    }
}
