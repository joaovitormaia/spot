﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrintWayy.SpotWayy.DAO.Musics;
using PrintWayy.SpotWayy.Entities;

namespace PrintWayy.SpotWayy.Business.Musics
{
    public class MusicServiceImpl : IMusicService
    {
        private IMusicsDAO _musicDAO;
        public IMusicsDAO MusicDao => _musicDAO;

        public List<Music> Select(Albums album)
        {
            return _musicDAO.Select(album);
        }
        public bool Add(List<Music> musicList)
        {
           foreach(var music in musicList)
           {
                bool CheckMusicExistence = _musicDAO.CheckExistence(music);
                if(CheckMusicExistence == false)
                {
                    return false;
                }
            }
            return _musicDAO.Add(musicList);
        }
        public bool Remove(Music musics)
        {
            return _musicDAO.Remove(musics);
        }

        public bool Edit(Music music)
        {
            bool CheckMusicExistence = _musicDAO.CheckExistence(music);
            if (CheckMusicExistence == false)
            {
                return false;
            }
            else
            {
                return _musicDAO.Edit(music);
            }
        }

        public MusicServiceImpl()
        {
            _musicDAO = new MusicsDAOImpl();
        }
        //encapsulamento, injeção de dependencia, inversão de dependencia,teste de unidade, solid
    }
}
