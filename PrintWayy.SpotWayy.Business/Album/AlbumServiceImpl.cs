﻿using PrintWayy.SpotWayy.Business.Musics;
using PrintWayy.SpotWayy.DAO.Album;
using PrintWayy.SpotWayy.DAO.Musics;
using PrintWayy.SpotWayy.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintWayy.SpotWayy.Business.Album
{
    public class AlbumServiceImpl: IAlbumService
    {
        private IAlbumDAO _albumDAO { get; }
        private IMusicsDAO _musicDAO { get; }
        public IMusicsDAO MusicDAO => _musicDAO;
        public IAlbumDAO AlbumDAO => _albumDAO;

        public List<Albums> Select()
        {
            List<Albums> AlbumsList = _albumDAO.Select();
            foreach(var album in AlbumsList)
            {
                album.MusicsList = _musicDAO.Select(album);
            }
            return AlbumsList;
        }

        public bool Edit(Albums album)
        {
            bool CheckIfAlbumAlreadyExists = CheckAlbumExistence(album);
            if(CheckIfAlbumAlreadyExists == true)
            {
                _albumDAO.Edit(album);
                return true;
            }
            return false;
        }

        public bool Add(Albums album)
        {
            bool CheckIfAlbumAlreadyExists = CheckAlbumExistence(album);
            if (CheckIfAlbumAlreadyExists == true)
            {
                _albumDAO.Add(album);
                return true;
            }
            return false;
        }

        public bool Remove(Albums album)
        {
            return _albumDAO.Remove(album);
        }

        public bool CheckAlbumExistence(Albums album)
        {
            int CheckIfAlbumAlreadyExists = _albumDAO.CheckAlbumExistence(album);
            if(CheckIfAlbumAlreadyExists >= 1)
            {
                return false;
            }
            return true;
        }

        public Albums SelectAlbumById(int AlbumId)
        {
            var album = _albumDAO.SelectAlbumById(AlbumId);
            album.MusicsList = _musicDAO.Select(album);
            return album;
        }

        public AlbumServiceImpl()
        {
            _albumDAO = new AlbumDAOImpl();
            _musicDAO = new MusicsDAOImpl();
        }
    }
}
