﻿using PrintWayy.SpotWayy.DAO.Album;
using PrintWayy.SpotWayy.DAO.Musics;
using PrintWayy.SpotWayy.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintWayy.SpotWayy.Business.Album
{
    public interface IAlbumService
    {
        IAlbumDAO AlbumDAO { get; }
        IMusicsDAO MusicDAO { get; }
        List<Albums> Select();
        bool Edit(Albums album);
        bool Add(Albums album);
        bool Remove(Albums album);
        bool CheckAlbumExistence(Albums album);
        Albums SelectAlbumById(int AlbumId);
    }
}
