﻿using AutoMapper;
using PrintWayy.SpotWayy.Business;
using PrintWayy.SpotWayy.Business.Album;
using PrintWayy.SpotWayy.Entities;
using PrintWayy.SpotWayy.SpotWayyApp.Helpers;
using PrintWayy.SpotWayy.SpotWayyApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrintWayy.SpotWayy.SpotWayyApp.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index(string nome)
        {
            var EntitiesList = AlbumObj.Select();
            List<Album> AlbumsList = MyMapper.Map<List<Album>>(EntitiesList);
            return View(AlbumsList);
        }

        public ActionResult ListAlbum()
        {
            
            return View();
        }
    }
}