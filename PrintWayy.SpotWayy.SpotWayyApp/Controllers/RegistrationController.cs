﻿using PrintWayy.SpotWayy.Entities;
using PrintWayy.SpotWayy.SpotWayyApp.Helpers;
using PrintWayy.SpotWayy.SpotWayyApp.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace PrintWayy.SpotWayy.SpotWayyApp.Controllers
{
    public class RegistrationController : BaseController
    {
        [HttpPost]
        public ActionResult Index(string button,int id)
        {
            if(button == "Editar")
            {
                return RedirectToAction(String.Format("EditAlbum/{0}",id));
            }
            return HttpNotFound();
        }
        public ActionResult NewAlbum()
        {
            return View();
        }
        public ActionResult EditAlbum(int? id)
        {
            int? albumId = id;
            if (albumId.HasValue)
            {
                var album = AlbumObj.SelectAlbumById(albumId.Value);
                foreach(var music in album.MusicsList)
                {
                    music.AlbumId = album.Id;
                }
                return View(album);
            }
            return HttpNotFound();
        }
        [HttpPost]
        public JsonResult NewAlbumPostValues(Album album)
        {
            Albums entitiesAlbum = MyMapper.Map<Albums>(album);
            bool CheckIfAlbumHasBeenAdded = AlbumObj.Add(entitiesAlbum);
            foreach(var music in entitiesAlbum.MusicsList)
            {
                music.AlbumId = entitiesAlbum.Id;
            }
            if(CheckIfAlbumHasBeenAdded == true)
            {
                MusicObj.Add(entitiesAlbum.MusicsList);
                return Json(new { success = "true" });
            }
            return Json(new { success = "false" });
        }
        [HttpPost]
        public JsonResult EditAlbumPostValues(Album album)
        {
            Albums entitiesAlbum = MyMapper.Map<Albums>(album);
            AlbumObj.Edit(entitiesAlbum);
            foreach (var music in entitiesAlbum.MusicsList)
            {
                music.AlbumId = entitiesAlbum.Id;
            }
            MusicObj.Add(entitiesAlbum.MusicsList);
            return Json(new { success = "true" });
        }
        [HttpPost]
        public JsonResult EditMusicPostValues(Entities.Music music)
        {
            MusicObj.Edit(music);
            return Json(new { success = "true" });
        }
        public PartialViewResult Header()
        {
            return PartialView();
        }
        public JsonResult api(int? Id)
        {
            List<Albums> albumList = new List<Albums>();
            if (Id.HasValue)
            {
                albumList.Add(AlbumObj.SelectAlbumById(Id.Value));
                return Json(albumList, JsonRequestBehavior.AllowGet);
            }
            albumList = AlbumObj.Select();
            return Json(albumList, JsonRequestBehavior.AllowGet);
        }
    }
}