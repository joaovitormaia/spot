﻿using PrintWayy.SpotWayy.Entities;
using PrintWayy.SpotWayy.SpotWayyApp.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrintWayy.SpotWayy.SpotWayyApp.Controllers
{
    public class DeleteController : BaseController
    {
        public ActionResult Album(int Id)
        {
            Albums album = new Albums { Id = Id };
            AlbumObj.Remove(album);
            return RedirectToAction("Index", "Home");
        }
        public ActionResult Music(int Id)
        {
            Music music = new Music { Id = Id };
            MusicObj.Remove(music);
            return View();
        }
    }
}