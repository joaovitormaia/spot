﻿//lista onde fica as musicas adicionadas pelo modal 
var AlbumList = []
var MusicList = []
var MusicsList = []
var MusicDict = {}
var divPattern = ""
var AlbumDict = {}
AlbumDict["MusicList"] = []
//event listeners para ordenar as musicas
$("#titulo_music")
$(document).ready(function () {
    document.getElementById('titulo_music').addEventListener('click', function () {
        /*
         
         explicação da função sort do js
         se a comparação for menor que zero, a é posicionado antes de b
         se a comparação for maior que zero, a é posicionado depois de b
         se a comparação for igual a zero, a e b permanecem com as posições inalteradas

         */
        MusicsList.sort((a, b) => (a.Name > b.Name) ? 1 : -1);
        document.getElementById('listofmusic').innerHTML = ''
        for (var i = 0; i < MusicsList.length; i++) {
            document.getElementById('listofmusic').innerHTML += '<div class="musiclist">\
                                                                    <div>\
                                                                        <p>'+ MusicsList[i].Name + '</p>\
                                                                    </div>\
                                                                    <div>\
                                                                        <p>'+ MusicsList[i].MusicGenre + '</p>\
                                                                    </div>\
                                                                    <div>\
                                                                        <p>'+ MusicsList[i].MusicLength + ' minutos</p>\
                                                                    </div>\
                                                                    <div>\
                                                                        <p>\
                                                                          <input type="button" value="Editar" class="buttonhome" onclick="openEditMusicModal('+ MusicsList[i].Id + ')" style="background-color:transparent;border:none" />\
                                                                          <input type="button" value="Excluir" class="buttonhome" onclick="document.getElementById(\'removeModal\').style.display = \'flex\'; document.getElementById(\'musicid\').value = '+ MusicsList[i].Id + '" style="background-color:transparent;border:none" />\
                                                                        </p >\
                                                                    </div>\
                                                                </div>'
        }
    });
    document.getElementById('genre_music').addEventListener('click', function () {
        MusicsList.sort((a, b) => (a.MusicGenre > b.MusicGenre) ? 1 : -1);
        document.getElementById('listofmusic').innerHTML = ''
        for (var i = 0; i < MusicsList.length; i++) {
            document.getElementById('listofmusic').innerHTML += '<div class="musiclist">\
                                                                    <div>\
                                                                        <p>'+ MusicsList[i].Name + '</p>\
                                                                    </div>\
                                                                    <div>\
                                                                        <p>'+ MusicsList[i].MusicGenre + '</p>\
                                                                    </div>\
                                                                    <div>\
                                                                        <p>'+ MusicsList[i].MusicLength + ' minutos</p>\
                                                                    </div>\
                                                                    <div>\
                                                                        <p>\
                                                                          <input type="button" value="Editar" class="buttonhome" onclick="openEditMusicModal('+ MusicsList[i].Id + ')" style="background-color:transparent;border:none" />\
                                                                          <input type="button" value="Excluir" class="buttonhome" onclick="document.getElementById(\'removeModal\').style.display = \'flex\'; document.getElementById(\'musicid\').value = '+ MusicsList[i].Id + '" style="background-color:transparent;border:none" />\
                                                                        </p >\
                                                                    </div>\
                                                                </div>'
        }
    });
    document.getElementById('length_music').addEventListener('click', function () {
        MusicsList.sort((a, b) => (a.MusicLength > b.MusicLength) ? 1 : -1);
        document.getElementById('listofmusic').innerHTML = '';
        for (var i = 0; i < MusicsList.length; i++) {
            document.getElementById('listofmusic').innerHTML += '<div class="musiclist">\
                                                                    <div>\
                                                                        <p>'+ MusicsList[i].Name + '</p>\
                                                                    </div>\
                                                                    <div>\
                                                                        <p>'+ MusicsList[i].MusicGenre + '</p>\
                                                                    </div>\
                                                                    <div>\
                                                                        <p>'+ MusicsList[i].MusicLength + ' minutos</p>\
                                                                    </div>\
                                                                    <div>\
                                                                        <p>\
                                                                          <input type="button" value="Editar" class="buttonhome" onclick="openEditMusicModal('+ MusicsList[i].Id + ')" style="background-color:transparent;border:none" />\
                                                                          <input type="button" value="Excluir" class="buttonhome" onclick="document.getElementById(\'removeModal\').style.display = \'flex\'; document.getElementById(\'musicid\').value = '+ MusicsList[i].Id + '" style="background-color:transparent;border:none" />\
                                                                        </p >\
                                                                    </div>\
                                                                </div>'
        }
    });
});

function EditAddMusicToList(Id) {
    if (checkIfMusicExists(document.getElementById('nome_musica').value) == true) {
        window.alert('Musica já existente na lista, por favor insira outro nome');
    }
    else if (isNaN(document.getElementById('duracao_musica').value)) {
        window.alert('Por favor,apenas números para a duração da musica');
    }
    else {
        AlbumDict = {
            "Id" : Id,
            "Name": document.getElementById('album_title').value,
            "AlbumImage": document.getElementById('image_url').value,
            "MusicList": []
        }
        MusicDict["Name"] = document.getElementById('nome_musica').value;
        MusicDict["MusicGenre"] = document.getElementById('genero_musica').value;
        MusicDict["MusicLength"] = document.getElementById('duracao_musica').value;
        MusicDict["AlbumId"] = Id;
        MusicList.push(MusicDict);
        AlbumDict["MusicList"] = MusicList;
        divPattern = '<div class="musiclist">\
                        <div>\
                            <p>'+ document.getElementById('nome_musica').value +'</p>\
                        </div>\
                        <div>\
                            <p>'+ document.getElementById('genero_musica').value +'</p>\
                        </div>\
                        <div>\
                            <p>'+ document.getElementById('duracao_musica').value +' minutos</p>\
                        </div>\
                        <div>\
                            <p>\
                            </p >\
                        </div>\
                      </div>'
        document.getElementById('listofmusic').innerHTML += divPattern;
        document.querySelector('.bg-modal').style.display = 'none';
        document.getElementById('nome_musica').value = "";
        document.getElementById('genero_musica').value = "";
        document.getElementById('duracao_musica').value = "";
        MusicDict = {}
    }
}
function NewAddMusicToList() {
    if (document.getElementById('nome_musica').value === "") {
        window.alert('Por favor adicione um nome a musica');
    }
    else if (document.getElementById('genero_musica').value === "") {
        window.alert('Por favor adicione um genero musical');
    }
    else if (document.getElementById('duracao_musica').value === "") {
        window.alert('Por favor adicione a duração da musica');
    }
    else if (checkIfMusicExists(document.getElementById('nome_musica').value) == true) {
        window.alert('Musica já existente na lista, por favor insira outro nome');
    }
    else if (isNaN(document.getElementById('duracao_musica').value)) {
        window.alert('Por favor,apenas números para a duração da musica');
    }
    else {
        AlbumDict = {
            "Name": document.getElementById('album_title').value,
            "AlbumImage": document.getElementById('image_url').value,
            "MusicList": []
        }
        MusicDict["Name"] = document.getElementById('nome_musica').value;
        MusicDict["MusicGenre"] = document.getElementById('genero_musica').value;
        MusicDict["MusicLength"] = document.getElementById('duracao_musica').value;
        MusicList.push(MusicDict);
        AlbumDict["MusicList"] = MusicList;
        divPattern = '<div class="musiclist">\
                        <div>\
                            <p>'+ document.getElementById('nome_musica').value + '</p>\
                        </div>\
                        <div>\
                            <p>'+ document.getElementById('genero_musica').value + '</p>\
                        </div>\
                        <div>\
                            <p>'+ document.getElementById('duracao_musica').value + '</p>\
                        </div>\
                        <div>\
                            <p></p>\
                        </div>\
                      </div>'
        document.getElementById('listofmusic').innerHTML += divPattern;
        document.querySelector('.bg-modal').style.display = 'none';
        document.getElementById('nome_musica').value = "";
        document.getElementById('genero_musica').value = "";
        document.getElementById('duracao_musica').value = "";
        MusicDict = {}
    }
}
function checkIfMusicExists(music) {
    for (var i = 0; i < MusicList.length; i++) {
        if (MusicList[i].Name == music) {
            return true;
        }
        return false;
    }
}
function openModalMusic() {
    document.querySelector('.bg-modal').style.display = 'flex';
    document.querySelector('.fecha').addEventListener('click', function () {
        document.querySelector('.bg-modal').style.display = 'none';
    });
}

function changeImg() {
    document.getElementById('img_music').src = document.getElementById('image_url').value
}
function ShowMusic() {
}

function NewSaveData(AlbumDict)
{
    $.ajax({
        type: "POST",
        url: "/Registration/NewAlbumPostValues",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(AlbumDict),
        success: function (result) {
            if (result.success) {
                alert("Album salvo com sucesso");
            } else {
                alert("Erro ao salvar os dados");
            }
        }
    });
}
function EditSaveData(AlbumDict,AlbumId) {
    $.ajax({
        type: "POST",
        url: "/Registration/EditAlbumPostValues",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(AlbumDict),
        success: function (result) {
            if (result.success) {
                alert("Album salvo com sucesso");
            } else {
                alert("Erro ao salvar os dados");
            }
        }
    });
}
function NewEnviaDados(MusicList) {
    if (document.getElementById('album_title').value == "") {
        window.alert('Por favor adicione um titulo ao album')
    }
    else if (MusicList.length <= 0) {
        window.alert('Por favor adicione ao menos uma música a lista abaixo')
    } else if (AlbumDict["MusicList"].length <= 0) {
        window.alert('Por favor adicione ao menos uma musica a lista abaixo')
    }
    else {
        NewSaveData(AlbumDict)
        window.location.href = "/"
    }
}
function EditEnviaDados(MusicList,AlbumId) {
    if (document.getElementById('album_title').value == "") {
        window.alert('Por favor adicione um titulo ao album')
    }
    else {
        AlbumDict["Id"] = AlbumId;
        AlbumDict["Name"] = document.getElementById('album_title').value;
        AlbumDict["AlbumImage"] = document.getElementById('image_url').value;
        EditSaveData(AlbumDict, AlbumId);
        window.location.href = "/";
    }
}
function removeAlbum(word, id) {
    if (word.toUpperCase() == "EXCLUIR") {
        $.ajax({
            type: "GET",
            url: "/Delete/Album/" + id,
            dataType: "json",
            contentType: "application/json",
            success: function (result) {
                if (result.success) {
                    alert("Album removido com sucesso");
                } else {
                    alert("Erro ao remover os dados");
                }
            }
        });
        window.location.reload();
    } else {
        window.alert('Digite a palavra correntamente para excluir o album');
    }
}
function showModal(id) {
    document.getElementById(id).style.display = "flex";
}
function openEditMusicModal(id) {
    document.getElementById('editmusicmodal').style.display = "flex";
    document.getElementById('musicId').value = id;
}
function EditMusicFromList() {
    if (document.getElementById('nome_musica_editmodal').value == "") {
        window.alert('Por favor insira um novo nome à musica')
    } else if (document.getElementById('genero_musica_editmodal').value == "") {
        window.alert('Por favor adicione um novo genero');
    } else if (document.getElementById('duracao_musica_editmodal').value == "" || isNaN(document.getElementById('duracao_musica_editmodal').value)) {
        window.alert('Por favor adicione um valor valido a duração')
    } else {
        var musicObject = {
            "Id": document.getElementById('musicId').value,
            "Name": document.getElementById('nome_musica_editmodal').value,
            "MusicGenre": document.getElementById('genero_musica_editmodal').value,
            "MusicLength": document.getElementById('duracao_musica_editmodal').value
        }
        $.ajax({
            type: "POST",
            url: "/Registration/EditMusicPostValues",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(musicObject),
            success: function (result) {
                if (result.success) {
                    alert("Album salvo com sucesso");
                } else {
                    alert("Erro ao salvar os dados");
                }
            }
        });
        document.getElementById('editmusicmodal').style.display = "none";
        window.location.reload();
    }
}
function removeMusic(word, musicId) {
    if (word.toUpperCase() == "EXCLUIR") {
        //window.location.href = "/Delete/Music/" + musicId;
        $.ajax({
            type: "GET",
            url: "/Delete/Music/" + musicId,
            dataType: "json",
            contentType: "application/json",
            success: function (result) {
                if (result.success) {
                    alert("Musica excluida com sucesso");
                } else {
                    alert("Erro ao excluir os dados");
                }
            }
        });
        window.location.reload();
    } else {
        window.alert("Escreva corretamente a palavra informada abaixo");
    }
}

function getItemsBoxAlbum(word) {
    $.get("/registration/api", function (data, status) {
        document.getElementById('allmusics').innerHTML = ''
        AlbumList = data;
        for (var i = 0; i < AlbumList.length; i++) {
            if (AlbumList[i].Name.includes(word)) {
                document.getElementById('allmusics').innerHTML += '<div>\
                                                           <p>'+ AlbumList[i].Name + '</p>\
                                                           <img src="'+ AlbumList[i].AlbumImage + '" onerror="this.onerror=null;this.src=\'https://utmsi.utexas.edu/components/com_easyblog/themes/wireframe/images/placeholder-image.png\'" class="album-image">\
                                                           <div class="withborder">\
                                                               <div><input type="submit" id="button_editar" value="Editar" class="buttonhome" onclick="window.location.href = \'/Registration/EditAlbum/'+ AlbumList[i].Id + '\'" /></div>\
                                                               <div><input type="submit" id="button_excluir" value="Excluir" class="buttonhome" onclick="showModal('+ AlbumList[i].Id + ')" /></div>\
                                                               <input type="hidden" name="id" value="'+ AlbumList[i].Id + '" />\
                                                           </div>\
                                                       </div>'
            }
        }
    });
}
function getItemsBox(id) {
    $.get("/registration/api/"+id, function (data) {
        document.getElementById('listofmusic').innerHTML = ''
        AlbumList = data;
        for (var i = 0; i < AlbumList.length; i++) {
            if (AlbumList[i].Name.includes(word)) {
                document.getElementById('listofmusic').innerHTML += '<div>\
                                                                       <p>'+ AlbumList[i].Name + '</p>\
                                                                       <img src="'+ AlbumList[i].AlbumImage + '" onerror="this.onerror=null;this.src=\'https://utmsi.utexas.edu/components/com_easyblog/themes/wireframe/images/placeholder-image.png\'" class="album-image">\
                                                                       <div class="withborder">\
                                                                           <div><input type="submit" id="button_editar" value="Editar" class="buttonhome" onclick="window.location.href = \'/Registration/EditAlbum/'+ AlbumList[i].Id + '\'" /></div>\
                                                                           <div><input type="submit" id="button_excluir" value="Excluir" class="buttonhome" onclick="showModal('+ AlbumList[i].Id + ')" /></div>\
                                                                           <input type="hidden" name="id" value="'+ AlbumList[i].Id + '" />\
                                                                       </div>\
                                                                   </div>'
            }
        }
    });
}
function showMusicOnEditAlbum(id) {
    $.get("/registration/api/" + id, function (data) {
        document.getElementById('listofmusic').innerHTML = ''
        AlbumList = data;
        MusicsList = AlbumList[0].MusicsList;
        for (var i = 0; i < MusicsList.length; i++) {
            document.getElementById('listofmusic').innerHTML += '<div class="musiclist">\
                                                                    <div>\
                                                                        <p>'+ MusicsList[i].Name + '</p>\
                                                                    </div>\
                                                                    <div>\
                                                                        <p>'+ MusicsList[i].MusicGenre + '</p>\
                                                                    </div>\
                                                                    <div>\
                                                                        <p>'+ MusicsList[i].MusicLength +' minutos</p>\
                                                                    </div>\
                                                                    <div>\
                                                                        <p>\
                                                                          <input type="button" value="Editar" class="buttonhome" onclick="openEditMusicModal('+ MusicsList[i].Id + ')" style="background-color:transparent;border:none" />\
                                                                          <input type="button" value="Excluir" class="buttonhome" onclick="document.getElementById(\'removeModal\').style.display = \'flex\'; document.getElementById(\'musicid\').value = '+ MusicsList[i].Id + '" style="background-color:transparent;border:none" />\
                                                                        </p >\
                                                                    </div>\
                                                                </div>'
        }
    });
}