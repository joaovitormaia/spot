﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrintWayy.SpotWayy.SpotWayyApp.Models
{
    public class Music
    {
        public string Name { get; set; }
        public string MusicGenre { get; set; }
        public string MusicLength { get; set; }
    }
}