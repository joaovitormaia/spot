﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PrintWayy.SpotWayy.SpotWayyApp.Models
{
    public class Album
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Music> MusicList { get; set; }
        public string AlbumImage { get; set; }
    }
}