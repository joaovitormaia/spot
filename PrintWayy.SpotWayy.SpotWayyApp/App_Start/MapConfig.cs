﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace PrintWayy.SpotWayy.SpotWayyApp.App_Start
{
    public class MapConfig
    {
        public static IMapper ConfigureMapper()
        {
             return new MapperConfiguration(cfg => {
                 cfg.CreateMap<Entities.Albums, Models.Album>()
                  .ForMember(dest => dest.MusicList, opt => opt.MapFrom(src => src.MusicsList));
                 cfg.CreateMap<Entities.Music, Models.Music>();
                 cfg.CreateMap<Models.Album, Entities.Albums>()
                 .ForMember(dest => dest.MusicsList, opt => opt.MapFrom(src => src.MusicList));
                 cfg.CreateMap<Models.Music, Entities.Music>();
            }).CreateMapper();
        }
        
    }
}