﻿using AutoMapper;
using PrintWayy.SpotWayy.Business.Album;
using PrintWayy.SpotWayy.Business.Musics;
using PrintWayy.SpotWayy.SpotWayyApp.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PrintWayy.SpotWayy.SpotWayyApp.Helpers
{
    public class BaseController : Controller
    {
        private IAlbumService _album = new AlbumServiceImpl();
        private IMusicService _music = new MusicServiceImpl();
        private IMapper _myMapper = MapConfig.ConfigureMapper();
        protected IMapper MyMapper => _myMapper;
        protected IAlbumService AlbumObj => _album;
        protected IMusicService MusicObj => _music;
    }
}