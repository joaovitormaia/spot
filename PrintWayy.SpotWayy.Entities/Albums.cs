﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintWayy.SpotWayy.Entities
{
    public class Albums
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AlbumImage { get; set; }
        public List<Music> MusicsList { get; set; }
        public int MusicsCount { get; set; }
    }
}
