﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrintWayy.SpotWayy.DAO.Musics;
using PrintWayy.SpotWayy.DAO.Util;
using PrintWayy.SpotWayy.Entities;

namespace PrintWayy.SpotWayy.DAO.Album
{
    public class AlbumDAOImpl : IAlbumDAO
    {
        public bool Add(Albums album)
        {
            try
            {
                using (SqlConnection connection = BDConnection.Connection())
                {
                    connection.Open();
                    String sql = String.Format("INSERT INTO Albums(name_album,image_url) VALUES('{0}','{1}') SELECT SCOPE_IDENTITY()", album.Name, album.AlbumImage);
                    SqlCommand command = new SqlCommand(sql, connection);
                    Int32 AlbumId = Convert.ToInt32(command.ExecuteScalar());
                    album.Id = AlbumId;
                    return true;
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
        }

        public List<Albums> Select()
        {
            List<Albums> AlbumsList = new List<Albums>();
            Albums album = new Albums();
            try
            {
                using (SqlConnection connection = BDConnection.Connection())
                {
                    connection.Open();
                    String sql = String.Format("SELECT name_album,al.id,COUNT(name_music),image_url FROM Albums al INNER JOIN Musics m ON(al.id = m.album_id) GROUP BY name_album,al.id,image_url;");
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                AlbumsList.Add(new Albums
                                {
                                    Name = reader.GetString(0),//nome album
                                    Id = reader.GetInt32(1),//id album
                                    MusicsCount = reader.GetInt32(2),
                                    AlbumImage = reader.GetString(3)
                                });
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
            return AlbumsList;
        }
        public bool Edit(Albums album)
        {
            try
            {
                using (SqlConnection connection = BDConnection.Connection())
                {
                    connection.Open();
                    String sql = String.Format("UPDATE Albums SET name_album = '{0}',image_url = '{1}' WHERE id = {2}", album.Name, album.AlbumImage, album.Id);
                    SqlCommand command = new SqlCommand(sql, connection);
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
        }

        public bool Remove(Albums album)
        {
            try
            {
                using (SqlConnection connection = BDConnection.Connection())
                {
                    connection.Open();
                    String sql = String.Format("DELETE FROM Albums WHERE id = {0}", album.Id);
                    SqlCommand command = new SqlCommand(sql, connection);
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
        }

        public int CheckAlbumExistence(Albums album)
        {
            try
            {
                using (SqlConnection connection = BDConnection.Connection())
                {
                    connection.Open();
                    String sql = String.Format("SELECT COUNT(*) FROM Albums WHERE name_album = '{0}'", album.Name);
                    SqlCommand command = new SqlCommand(sql, connection);
                    return Int32.Parse(command.ExecuteScalar().ToString());
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
        }

        public Albums SelectAlbumById(int AlbumId)
        {
            Albums album = new Albums();
            try
            {
                using (SqlConnection connection = BDConnection.Connection())
                {
                    connection.Open();
                    String sql = String.Format("SELECT name_album,al.id,COUNT(name_music),image_url FROM Albums al INNER JOIN Musics m ON(al.id = m.album_id) WHERE al.id = {0} GROUP BY name_album,al.id,image_url;", AlbumId);
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                album.Name = reader.GetString(0);
                                album.Id = reader.GetInt32(1);
                                album.MusicsCount = reader.GetInt32(2);
                                album.AlbumImage = reader.GetString(3);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
            return album;
        }
    }
}
