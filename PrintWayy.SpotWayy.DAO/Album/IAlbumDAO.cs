﻿using PrintWayy.SpotWayy.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintWayy.SpotWayy.DAO.Album
{
    public interface IAlbumDAO
    {
        List<Albums> Select();
        Albums SelectAlbumById(int AlbumId);
        bool Edit(Albums album);
        bool Add(Albums album);
        bool Remove(Albums album);
        int CheckAlbumExistence(Albums album);
    }
}
