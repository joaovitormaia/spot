﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrintWayy.SpotWayy.DAO.Util;
using PrintWayy.SpotWayy.Entities;

namespace PrintWayy.SpotWayy.DAO.Musics
{
    public class MusicsDAOImpl : IMusicsDAO
    {
        public bool Add(List<Music> musicList)
        {
            try
            {
                using (SqlConnection connection = BDConnection.Connection())
                {
                    connection.Open();
                    List<Music> MusicList = musicList;
                    int CountingOfSongsInTheList = MusicList.Count();
                    if(CountingOfSongsInTheList <= 0)
                    {
                        return false;
                    }
                    String sql = String.Format("INSERT INTO Musics(name_music,album_id,music_length,music_genre) VALUES");
                    for (int i = 0; i < CountingOfSongsInTheList; i++)
                    {
                        if (i == (CountingOfSongsInTheList - 1))
                        {
                            sql += String.Format("('{0}',{1},{2},'{3}');", MusicList[i].Name, MusicList[i].AlbumId, MusicList[i].MusicLength,MusicList[i].MusicGenre);
                        }
                        else
                        {
                            sql += String.Format("('{0}',{1},{2},'{3}'),", MusicList[i].Name, MusicList[i].AlbumId, MusicList[i].MusicLength, MusicList[i].MusicGenre);
                        }
                    }
                    SqlCommand command = new SqlCommand(sql, connection);
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
        }
        public bool Remove(Music music)
        {
            try
            {
                using (SqlConnection connection = BDConnection.Connection())
                {
                    connection.Open();
                    String sql = String.Format("DELETE FROM Musics WHERE id = {0}", music.Id);
                    SqlCommand command = new SqlCommand(sql, connection);
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
        }
        public List<Music> Select(Albums album)
        {
            List<Music> MusicList = new List<Music>();
            try
            {
                using (SqlConnection connection = BDConnection.Connection())
                {
                    connection.Open();
                    String sql = String.Format("SELECT m.id,name_music,music_length,music_genre,album_id FROM Musics m INNER JOIN Albums al ON(m.album_id = al.id) WHERE album_id = {0}", album.Id);
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                MusicList.Add(new Music {
                                    Id = reader.GetInt32(0),
                                    Name = reader.GetString(1),
                                    MusicLength = reader.GetInt32(2),
                                    MusicGenre = reader.GetString(3),
                                    AlbumId = reader.GetInt32(4)
                                });
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
            return MusicList;
        }
        public bool Edit(Music music)
        {
            try
            {
                using (SqlConnection connection = BDConnection.Connection())
                {
                    connection.Open();
                    String sql = String.Format("UPDATE Musics SET name_music = '{0}', music_genre = '{1}', music_length = {2}  FROM Musics WHERE id = {3}", music.Name, music.MusicGenre, music.MusicLength, music.Id);
                    SqlCommand command = new SqlCommand(sql, connection);
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
        }

        public bool CheckExistence(Music music)
        {
            try
            {
                using (SqlConnection connection = BDConnection.Connection())
                {
                    music.Name.Replace("\'", "");
                    connection.Open();
                    String sql = String.Format("SELECT COUNT(name_music) FROM Musics m INNER JOIN Albums al ON(m.album_id = al.id) WHERE al.id = {0} AND name_music = '{1}'", music.AlbumId, music.Name);
                    SqlCommand command = new SqlCommand(sql, connection);
                    Int32 AlbumId = Convert.ToInt32(command.ExecuteScalar());
                    if(AlbumId >= 1)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
        }
    }
}
