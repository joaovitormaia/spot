﻿using PrintWayy.SpotWayy.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintWayy.SpotWayy.DAO.Musics
{
    public interface IMusicsDAO
    {
        List<Music> Select(Albums album);
        bool Add(List<Music> musicList);
        bool Remove(Music musics);
        bool Edit(Music music);
        bool CheckExistence(Music music);//checa se a musica ja existe no banco e no album
    }
}
